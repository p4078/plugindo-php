<div align="center">
<h1>PLUGINDO</h1>
<img src="assets/apindo_logo.png" alt="plugindo logo" width="80px" height="80px">
</div>

<h2 id="desc">Presentation</h2>
<p>
This project is a plugin for any project for requesting products from europe nintendo eshop

the official website: https://www.nintendo.com/games/
</p>

<h2 id="desc">Summary</h2>
<ol>
    <li><h3>Requirements</h3></li>
    <li><h3>How to use</h3></li>
    <li><h3>Items Retrievable</h3></li>
    <li><h3>Set Request Parameters</h3></li>
</ol>

<h2>Requirements</h2>

<table>
<caption>Requirements</caption>
<thead>
    <th>Techno</th>
    <th>Minimum Version</th>
    <th>Library Required</th>
    <th>Plugin url</th>
</thead>
<tbody>
    <tr>
        <td>PHP</td>
        <td> >= 7.4 </td>
        <td>
            - curl
        </td>
        <td>https://packagist.org/packages/plugindo/plugindo</td>
    </tr>
</tbody>
</table>

<h2 id="items">How to use</h2>
<p>
The way for requesting items is the same on all languages.<br>
For requesting something you have to do call your items like this :
</p>

````php
require 'vendor/autoload.php';
use Plugindo\Games;

//================ BASIC USAGE ================
// type of collection you want to get
// always take the plural filename like Games and NOT Game
$typeOfCollection = new Games();

// get products for the collection
$typeOfCollection->get();

// access to the game get
var_dump($typeOfCollection->getGames());

//================ CUSTOM LANGUAGE ================
// default language is English
// in this case I set it to French
$typeOfCollection = new Games('fr');

//================ SET PARAMETERS ================
// here I set parameter with a searched by keywords
// available parameters are described below
$parameters = [
    'q' => 'mario kart',
];

$typeOfCollection = new Games('fr');

$typeOfCollection->get($parameters);
var_dump($typeOfCollection->getGames());
````

<h2 id="items">Items Retrievable</h2> 

<table>
<caption>Items</caption>
<thead>
    <th>Name</th>
    <th>Class You Have To Call</th>
</thead>
<tbody>
    <tr>
        <td>Games</td>
        <td>Games</td>
    </tr>
    <tr>
        <td>Nintendo news</td>
        <td>News</td>
    </tr>
    <tr>
        <td>Amiibo</td>
        <td>Amiibos</td>
    </tr>
    <tr>
        <td>Merchandising</td>
        <td>Merchandises</td>
    </tr>
</tbody>
</table>

<h2 id="params">Set parameters for your request</h3>

<p>
You can pass parameters for getting precise result, if any params dont match with what <br>
is described below you will get an Exception for avoiding error during request <br>
execution
 <br>
 <br>
You can find an example of how set parameter on top of this readme
</p>

<table>
<caption>Parameters</caption>
<thead>
    <th>Key</th>
    <th>Value Type</th>
    <th>Default Value if not set</th>
    <th>Desc</th>
</thead>
<tbody>
    <tr>
        <td>rows</td>
        <td>int</td>
        <td>5</td>
        <td>amount of result you want</td>
    </tr>
    <tr>
        <td>q</td>
        <td>string</td>
        <td>*</td>
        <td>set keywords the product name include</td>
    </tr>
    <tr>
        <td>fq</td>
        <td>string[]</td>
        <td>[]</td>
        <td>You must have a syntax like this "keys:value" or "keys_name:value"</td>
    </tr>
</tbody>
</table>

<h2>Language Available</h2>
<table>
<caption>Language available</caption>
<thead>
    <th>language</th>
    <th>key to use</th>
</thead>
<tbody>
    <tr>
        <td>English</td>
        <td>en</td>
    </tr>
    <tr>
        <td>French</td>
        <td>fr</td>
    </tr>
    <tr>
        <td>German</td>
        <td>de</td>
    </tr>
    <tr>
        <td>Spanish</td>
        <td>es</td>
    </tr>
    <tr>
        <td>Italian</td>
        <td>it</td>
    </tr>
    <tr>
        <td>Russian</td>
        <td>ru</td>
    </tr>
    <tr>
        <td>Portuguese</td>
        <td>pt</td>
    </tr>
    <tr>
        <td>Dutch</td>
        <td>du</td>
    </tr>
</tbody>
</table>

<h2>License</h2>
GNU GENERAL PUBLIC LICENSE Version 3

<h2>Project Status</h2>
this project is in development to be available on composer and next as a python package atm
