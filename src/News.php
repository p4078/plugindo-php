<?php
namespace Plugindo;

include_once 'API.php';
include_once 'SingleNews.php';

class News extends API
{
    private array $news;

    public function __construct(string $language = 'en')
    {
        parent::__construct($language);
    }

    /**
     * @throws Exception
     */
    public function get(array $parameters = [])
    {
        $parameters[ApiParameters::KEY_PRODUCT_ATTRIBUTE][] = 'type:NEWS';
        $news = $this->request($parameters);

        foreach ($news as $singleNews){
            // set element we want to get
            $name = $singleNews['title']??(string)null;
            $resume = $singleNews['excerpt']??(string)null;
            $img = $singleNews['image_url']??(string)null;

            //create new item a game in this case
            $newGame = new SingleNews($name, $resume, $img);

            $this->news[] = $newGame;
        }
    }

    /**
     * @return array
     */
    public function getNews(): array
    {
        return $this->news;
    }
}