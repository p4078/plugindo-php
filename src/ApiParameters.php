<?php
namespace Plugindo;

class ApiParameters
{
    // parameter key accepted
    public const KEY_AMOUNT_RESULT_GET = 'rows';
    public const KEY_PRODUCT_ATTRIBUTE = 'fq';
    public const KEY_KEYWORD = 'q';
    public const KEY_PRODUCT_AMOUNT_START = 'start';

    //parameters default value
    public const DEFAULT_VALUE_AMOUNT_RESULT_GET = 5;
    public const DEFAULT_VALUE_KEYWORD = '*';
    public const DEFAULT_VALUE_PRODUCT_ATTRIBUTE = [];
    public const DEFAULT_VALUE_PRODUCT_AMOUNT_START = 0;

    // type await for each parameter
    public const TYPE_AWAIT_AMOUNT_RESULT_START = 'integer';
    public const TYPE_AWAIT_AMOUNT_RESULT_GET = 'integer';
    public const TYPE_AWAIT_PRODUCT_ATTRIBUTE = 'array';
    public const TYPE_AWAIT_KEYWORD = 'string';

    // value for product attribute
    public const PATTERN_PRODUCT_ATTRIBUTE = '/^[a-z|_]+:[aA0-zZ9]+$/m';

    /**
     * check if all given parameters key area available
     * @param array $parameters
     * @throws Exception
     */
    public static function validateParametersKeys(array $parameters){
        $keys = array_keys($parameters);
        if (!empty($parameters)){
            // check if keys are allowed
            foreach ($keys as $key){
                switch ($key){
                    case self::KEY_PRODUCT_AMOUNT_START:
                        throw new Exception('you cant pass the key '.$key);

                    case self::KEY_PRODUCT_ATTRIBUTE:
                    case self::KEY_KEYWORD:
                    case self::KEY_AMOUNT_RESULT_GET:
                        break;

                    default:
                        unset($parameters[$key]);
                        break;
                }
            }
        }
    }

    /**
     * check if all values passed in parameters
     * are correct
     * @param array $parameters
     * @throws Exception
     * @throws ValueError
     */
    public static function validateParametersValues(array $parameters){
        foreach ($parameters as $key=>$value) {
            $valueType = gettype($value);
            switch ($key){
                case self::KEY_PRODUCT_AMOUNT_START:
                    if ($valueType !== self::TYPE_AWAIT_AMOUNT_RESULT_START){
                        self::throwParameterTypeException(self::TYPE_AWAIT_AMOUNT_RESULT_START, $valueType);
                    }
                    if ($value < self::DEFAULT_VALUE_PRODUCT_AMOUNT_START){
                        throw new Exception('Value cant be less than '.self::DEFAULT_VALUE_PRODUCT_AMOUNT_START);
                    }
                    break;

                case self::KEY_AMOUNT_RESULT_GET:
                    if ($valueType !== self::TYPE_AWAIT_AMOUNT_RESULT_GET){
                        self::throwParameterTypeException(self::TYPE_AWAIT_AMOUNT_RESULT_GET, $valueType);
                    }
                    if ($value < self::DEFAULT_VALUE_PRODUCT_AMOUNT_START){
                        throw new Exception('Value cant be less than '.self::DEFAULT_VALUE_PRODUCT_AMOUNT_START);
                    }
                    break;

                case self::KEY_KEYWORD:
                    if ($valueType !== self::TYPE_AWAIT_KEYWORD){
                        self::throwParameterTypeException(self::TYPE_AWAIT_KEYWORD, $valueType);
                    }
                    break;

                case self::KEY_PRODUCT_ATTRIBUTE:
                    if ($valueType !== self::TYPE_AWAIT_PRODUCT_ATTRIBUTE){
                        self::throwParameterTypeException(self::TYPE_AWAIT_PRODUCT_ATTRIBUTE, $valueType);
                    }
                    if (!empty($value)){
                        foreach ($value as $attribute){
                            if (preg_match_all(self::PATTERN_PRODUCT_ATTRIBUTE, $attribute) === 0){
                                self::throwParameterPatternException($attribute);
                            }
                        }
                    }
                    break;

                default:
                    throw new Exception('Key '.$key.' is not available');
            }
        }
    }

    /**
     * TODO:
     *  - check for key attribute
     *
     * set required parameters who don't exist with default value
     * @param array $parameters
     * @return array
     */
    public static function setParameters(array &$parameters): array{
        $keys = array_keys($parameters);

        if (!in_array(self::KEY_KEYWORD, $keys, true)){
            $parameters[self::KEY_KEYWORD] = self::DEFAULT_VALUE_KEYWORD;
        }

        if (!in_array(self::KEY_PRODUCT_AMOUNT_START, $keys, true)){
            $parameters[self::KEY_PRODUCT_AMOUNT_START] = self::DEFAULT_VALUE_PRODUCT_AMOUNT_START;
        }

        if (!in_array(self::KEY_AMOUNT_RESULT_GET, $keys, true)){
            $parameters[self::KEY_AMOUNT_RESULT_GET] = self::DEFAULT_VALUE_AMOUNT_RESULT_GET;
        }

        return $parameters;
    }

    /**
     * build parameter for being understandable for url
     *
     * @param array $parameters
     * @return string
     */
    public static function buildParameterUrl(array $parameters): string{
        $url = '';

        foreach ($parameters as $key=>$value){
            $type = gettype($value);
            $url .= '&'.$key.'=';

            if ($type === 'array'){
                foreach ($value as $item) {
                    $url .= $item.', ';
                }
            }else{
                $url .= $value;
            }
        }
        return substr($url, 1);
    }

    /**
     * throw error for parameters value type
     *
     * @throws Exception
     */
    public static function throwParameterTypeException(string $typeAwait, string $currentType){
        throw new Exception('Type must be '.$typeAwait.' '.$currentType.' given');
    }

    /**
     * throw error for parameters value type
     *
     * @throws ValueError
     * @throws Exception
     */
    public static function throwParameterPatternException(string $value){
        throw new Exception('Value: '.$value.' has an invalid syntax you must respect this pattern: '.self::PATTERN_PRODUCT_ATTRIBUTE);
    }
}