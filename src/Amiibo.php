<?php
namespace Plugindo;

include_once 'Product.php';

class Amiibo extends Product
{
    protected float $price;
    protected string $name;
    protected string $resume;
    protected string $img;

    //custom attribute if you want more than basic override ahead
    private array $compatibleGameID;

    /**
     * @param float $price
     * @param string $name
     * @param string $resume
     * @param string $img
     * @param array $compatibleGameID
     */
    public function __construct(float $price, string $name, string $resume, string $img, array $compatibleGameID)
    {
        $this->price = $price;
        $this->name = $name;
        $this->resume = $resume;
        $this->img = $img;
        $this->compatibleGameID = $compatibleGameID;
    }


    /**
     * @return array
     */
    public function getCompatibleGameID(): array
    {
        return $this->compatibleGameID;
    }
}