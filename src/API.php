<?php
namespace Plugindo;

include_once 'ApiLanguage.php';
include_once 'ApiParameters.php';

abstract class API
{
    protected const ENTRYPOINT = "https://searching.nintendo-europe.com/";
    protected const ENDPOINT = 'select?';
    protected string $language;

    /**
     * override it for parse and stock product
     *
     * @return mixed
     */
    abstract public function get(array $parameters);

    /**
     * perform request on nintendo eshop
     *
     * @param array $parameters
     * @return array
     * @throws Exception
     */
    protected function request(array $parameters): array{
        // check parameters, set default value for required param if not set, and validate values
        ApiParameters::validateParametersKeys($parameters);
        $parameters = ApiParameters::setParameters($parameters);
        ApiParameters::validateParametersValues($parameters);

        //build url
        $url = self::ENTRYPOINT.$this->language.'/'.self::ENDPOINT.ApiParameters::buildParameterUrl($parameters);

        // init curl module to make request
        $request = curl_init($url);

        // prepare request
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($request, CURLOPT_PROXY_SSL_VERIFYHOST, FALSE);

        // execute request
        $response = curl_exec($request);

        // parse response as an associative array
        $response = json_decode($response, true);
        curl_close($request);

        return $response['response']['docs'];
    }

    /**
     * @param string $language
     */
    public function __construct(string $language)
    {
        ApiLanguage::checkIfExist($language);
        $this->language = $language;
    }

    /**
     * build parameters from array to string
     * for url
     *
     * @param array $parameters
     * @return string
     */
    private function parametersToString(array $parameters): string{
        return '';
    }
}