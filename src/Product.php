<?php
namespace Plugindo;

abstract class Product
{
    protected float $price;
    protected string $name;
    protected string $resume;
    protected string $img;


    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getResume(): string
    {
        return $this->resume;
    }

    /**
     * @return string
     */
    public function getImg(): string
    {
        return $this->img;
    }


}