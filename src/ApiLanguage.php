<?php
namespace Plugindo;

class ApiLanguage
{
    public const FRENCH = "fr";
    public const ENGLISH = "en";
    public const GERMAN = "de";
    public const SPANISH = "es";
    public const ITALIAN = "it";
    public const RUSSIAN = "ru";
    public const PORTUGUESE = "pt";
    public const DUTCH = "nl";

    /**
     * check if the language given is available
     *
     * @param string $language
     */
    public static function checkIfExist(string $language){
        switch ($language){
            case self::ENGLISH:
            case self::SPANISH:
            case self::GERMAN:
            case self::ITALIAN:
            case self::RUSSIAN:
            case self::PORTUGUESE:
            case self::DUTCH:
            case self::FRENCH:
                break;

            default:
                throw new ValueError('Given language incorrect');
        }
    }
}