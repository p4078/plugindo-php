<?php
namespace Plugindo;

include_once 'API.php';
include_once 'Amiibo.php';

class Amiibos extends API
{
    private array $amiibos;

    public function __construct(string $language = 'en')
    {
        parent::__construct($language);
    }

    /**
     * @throws Exception
     */
    public function get(array $parameters = [])
    {
        $parameters[ApiParameters::KEY_PRODUCT_ATTRIBUTE][] = 'type:FIGURE';
        $figures = $this->request($parameters);

        foreach ($figures as $figure){
            // set element we want to get
            $price = $figure['price_regular_f']??(float)null;
            $name = $figure['title']??(string)null;
            $resume = $figure['excerpt']??(string)null;
            $img = $figure['image_url']??(string)null;
            $compatibleGameID = $figure['compatible_games_list_id_txt']??(array)null;

            //create new item a game in this case
            $newFigure = new Amiibo($price, $name, $resume, $img, $compatibleGameID);

            $this->amiibos[] = $newFigure;
        }
    }

    /**
     * @return array
     */
    public function getAmiibos(): array
    {
        return $this->amiibos;
    }

}