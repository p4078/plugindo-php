<?php
namespace Plugindo;

include_once 'Product.php';

class SingleNews extends Product
{
    protected string $name;
    protected string $resume;
    protected string $img;

    /**
     * @param string $name
     * @param string $resume
     * @param string $img
     */
    public function __construct(string $name, string $resume, string $img)
    {
        $this->name = $name;
        $this->resume = $resume;
        $this->img = $img;
    }
}