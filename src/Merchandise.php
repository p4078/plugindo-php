<?php
namespace Plugindo;

include_once 'Product.php';

class Merchandise extends Product
{
    protected float $price;
    protected string $name;
    protected string $resume;
    protected string $img;

    /**
     * @param float $price
     * @param string $name
     * @param string $resume
     * @param string $img
     */
    public function __construct(float $price, string $name, string $resume, string $img)
    {
        $this->name = $price;
        $this->name = $name;
        $this->resume = $resume;
        $this->img = $img;
    }
}