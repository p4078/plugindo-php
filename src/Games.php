<?php
namespace Plugindo;

include_once 'API.php';
include_once 'Game.php';

class Games extends API
{
    private array $games;

    public function __construct(string $language = 'en')
    {
        parent::__construct($language);
    }

    /**
     * @throws Exception
     */
    public function get(array $parameters = [])
    {
        $parameters[ApiParameters::KEY_PRODUCT_ATTRIBUTE][] = 'type:GAME';
        $games = $this->request($parameters);

        foreach ($games as $game){
            // set element we want to get
            $price = $game['price_regular_f']??(float)null;
            $name = $game['title']??(string)null;
            $resume = $game['excerpt']??(string)null;
            $img = $game['image_url']??(string)null;

            //create new item a game in this case
            $newGame = new Game($price, $name, $resume, $img);

            $this->games[] = $newGame;
        }
    }

    /**
     * @return array
     */
    public function getGames(): array
    {
        return $this->games;
    }

}