<?php
namespace Plugindo;

class Merchandises extends API
{
    private array $merchandises;

    public function __construct(string $language = 'en')
    {
        parent::__construct($language);
    }

    /**
     * @throws Exception
     */
    public function get(array $parameters = [])
    {
        $parameters[ApiParameters::KEY_PRODUCT_ATTRIBUTE][] = 'type:MERCHANDISE';
        $merchandises = $this->request($parameters);

        foreach ($merchandises as $merchandise){
            // set element we want to get
            $price = $merchandise['price_regular_f']??(float)null;
            $name = $merchandise['title']??(string)null;
            $resume = $merchandise['excerpt']??(string)null;
            $img = $merchandise['image_url']??(string)null;

            //create new item a game in this case
            $newGame = new Game($price, $name, $resume, $img);

            $this->merchandises[] = $newGame;
        }
    }

    /**
     * @return array
     */
    public function getMerchandises(): array
    {
        return $this->merchandises;
    }
}